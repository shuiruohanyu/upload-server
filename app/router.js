'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  router.post('/upload', controller.home.upload);
  router.post('/checkFile', controller.home.checkFileUpload);
  router.post('/mergeFile', controller.home.mergeFile);
};
